const quotes = [
  {
    author: 'Stromberg',
    id: 1,
    text: 'Auch mein Fass hat Grenzen. Und wenn\'s da ständig reintropft, dann ist irgendwann der Boden raus!',
  },
  {
    author: 'The Dumbsternerd',
    id: 2,
    text: 'Linux hat den dicksten Arsch!',
  },
];

const quote = {
  author: 'The Interwebs',
  id: 3,
  text: 'Always code as if the person who ends up maintaining your code is a violent psychopath who knows where you live.',
};

export async function fetchQuotes(): Promise<Quote[]> {
  return quotes;
}

export async function fetchSingleQuote(): Promise<Quote> {
  return quote;
}

export async function createQuote(newQuote: Quote): Promise<Quote> {
  return newQuote;
}

export async function updateQuote(quoteId: number, updatedData: Partial<Quote>): Promise<Quote> {
  return { id: quoteId, ...updatedData } as Quote;
}

export async function deleteQuote(quoteId: number): Promise<boolean> {
  return true;
}
