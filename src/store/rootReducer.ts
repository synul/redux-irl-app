import { useSelector, TypedUseSelectorHook } from 'react-redux';
import { combineReducers } from '@reduxjs/toolkit';

import { notifications } from './notifications';
import { quotes } from './quotes';
import { stats } from './stats';

export const rootReducer = combineReducers({
    notifications,
    quotes,
    stats,
});

// This would be the root state of an app.
export type RootAppState = ReturnType<typeof rootReducer>;

// Selector hook that is specialized for a certain app state.
export const specializedSelector: TypedUseSelectorHook<RootAppState> = useSelector;
