import {
  createRepo,
  ReduceOperation,
} from 'redux-irl';

import * as api from '../../api';
import { RootAppState } from '../rootReducer';


const quotesRepo = createRepo<Quote>({
  name: 'quotes',
  selectId: quote => quote.id,
});


export const fetchQuotes = (arg: any) => {
  return quotesRepo.fetch({
    fetchFn: api.fetchQuotes,
    operation: ReduceOperation.AddMany,
  });
};

export const fetchSingleQuote = (quoteId: number) => {
  return quotesRepo.fetch({
    fetchFn: api.fetchSingleQuote,
    operation: ReduceOperation.AddOne,
  });
};


export const actions = quotesRepo.slice.actions;
export const quotes = quotesRepo.slice.reducer;
export const selectors = quotesRepo.adapter.getSelectors((state: RootAppState) => state.quotes);
export const quotesSelector = selectors.selectAll;