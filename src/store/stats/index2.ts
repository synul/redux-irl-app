import { createSlice } from '@reduxjs/toolkit';

import { RootAppState } from '../rootReducer';

import { actions as quotesActions } from '../quotes';

type Stats = {
  fetchCount: number;
}

const initialState: Stats = {
  fetchCount: 0,
};

const statsSlice = createSlice({
  name: 'stats',
  initialState,
  reducers: {},
  extraReducers: {
    [quotesActions.setPending.type]: (state, action) => {
      state.fetchCount += 1;
    },
  },
});

export function statsSelector(state: RootAppState) {
  return state.stats;
}

export const actions = statsSlice.actions;
export const stats = statsSlice.reducer;