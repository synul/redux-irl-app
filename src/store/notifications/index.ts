import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootAppState } from '../rootReducer';


type Notification = {
  id: string;
  message: string;
  title: string;
  type: 'error' | 'info';
}

const initialState: Notification[] = [];

export const notificationsSlice = createSlice({
  name: 'notifications',
  initialState,
  reducers: {
    open: (state, action: PayloadAction<Notification>) => {
      state.push({
        ...action.payload,
      });
    },
    closeLast: (state) => {
      state.pop();
    },
    close: (state, action: PayloadAction<string>) => {
      return state.filter(notification => notification.id !== action.payload);
    }
  }
});

export function selectNotifications(state: RootAppState) {
  return state.notifications;
}

export const actions = notificationsSlice.actions;
export const notifications = notificationsSlice.reducer;