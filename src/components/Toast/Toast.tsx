import React from 'react';

import { actions as notificationActions } from '../../store/notifications';

import { ToastStyled } from './Toast.styled';
import { useDispatch } from 'react-redux';


type Props = {
  id: string;
  message: string;
  title: string;
  type: 'error' | 'info';
}


export function Toast({
  id,
  message,
  title,
  type,
}: Props) {
  const dispatch = useDispatch();

  const close = () => {
    dispatch(notificationActions.close(id));
  }

  return (
    <ToastStyled>
      <header>
        <h4>{title}</h4>
        <button className="btn-close" onClick={close}>x</button>
      </header>
      <p>{message}</p>
    </ToastStyled>
  )
}