import styled from 'styled-components';

export const QuoteStyled = styled.div`
  margin: 0 0 20px 0;
  padding: 20px;
  border: 1px solid gray;
  border-radius: 5px;

  .text {
    font-size: 18px;
  }

  .author {
    font-size: 14px;
  }
`;