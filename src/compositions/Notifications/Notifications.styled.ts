import styled from 'styled-components';


export const NotificationsStyled = styled.div`
  position: absolute;
  top: 10px;
  right: 10px;
`;