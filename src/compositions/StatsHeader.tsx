import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';

import { statsSelector } from '../store/stats';

import {
  fetchQuotes,
} from '../store/quotes';
import { useReduxDispatch } from '../store';


export function StatsHeader() {
  const dispatch = useReduxDispatch();

  useEffect(() => {
    dispatch(fetchQuotes(1));
  }, [])


  const stats = useSelector(statsSelector);

  return (
    <>
      <h1>Fetch Count: {stats.fetchCount} </h1>
    </>
  );
}