import React from 'react';
import { connect, ConnectedProps } from 'react-redux';
import { Dispatch, bindActionCreators } from '@reduxjs/toolkit';

import { RootAppState } from '../store/rootReducer';
import {
  fetchQuotes,
  fetchSingleQuote,
  quotesSelector,
} from '../store/quotes';

import { Quote } from '../components/Quote';


type ReduxProps = ConnectedProps<typeof connector>;

interface Props extends ReduxProps {
  someOtherProp?: string;
}

const mapState = (state: RootAppState) => ({
  quotes: quotesSelector(state),
});

const mapDispatch = (dispatch: Dispatch) => bindActionCreators({
  fetchQuotes,
  fetchSingleQuote,
}, dispatch);

const connector = connect(mapState, mapDispatch);


class MyClassCompImpl extends React.Component<Props> {

  componentDidMount() {
    this.props.fetchQuotes(true)
      .then(res => {
        // console.log(res);
      })
      .catch(err => console.error(err));
  }

  handleClick = () => {
    this.props.fetchSingleQuote(3);
  }

  handleClickAll = () => {
    this.props.fetchQuotes(true)
  }

  render() {
    // console.log('RENDER: ', this.props);
    return (
      <>
        <h1>Quotes</h1>
        <>
          {this.props.quotes.map(quote => {
            return (
              <Quote key={quote.id} {...quote} />
            )
          })}
        </>
        <button onClick={this.handleClick}>Fetch Single Quote</button>
        <button onClick={this.handleClickAll}>Fetch All Quotes</button>
      </>
    );
  }
}

export const MyClassComp = connect(
  mapState,
  mapDispatch,
)(MyClassCompImpl);
