import { createGlobalStyle } from 'styled-components';

export const GlobalStyles = createGlobalStyle`
  body {
    padding: 20px;
  }

  p, h1, h2, h3, h4 {
    margin: 0;
    padding: 0;
    color: dimgray;
    font-family: Arial;
  }

  h1, h2, h3, h4 {
    margin: 10px 0 15px 0;
  }
`;
